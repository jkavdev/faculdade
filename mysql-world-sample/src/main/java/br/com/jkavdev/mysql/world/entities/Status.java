package br.com.jkavdev.mysql.world.entities;

import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

public enum Status {

	OFFICIAL("T", "Official"), UNOFFICIAL("F", "Unofficial");

	private static final Map<String, Status> valueMap;
	private String value;
	private String description;

	static {
		//oia, foi, mas acho que tem como melhorar
		valueMap = Collections.unmodifiableMap(
				Stream.of(Status.values())
				.collect(toMap(Status::value, Function.identity())));
	}

	private Status(String value, String description) {
		this.value = value;
		this.description = description;
	}

	public static Status of(String value) {
		if(isBlank(value)) throw new IllegalArgumentException("Unknown value: " + value);
		return valueMap.get(value);
	}

	public String value() {
		return value;
	}
	public String description() {
		return description;
	}

}
