package br.com.jkavdev.mysql.world.entities;

import static org.apache.commons.lang3.StringUtils.*;

public enum Continent {
	
	ASIA("Asia"),
	EUROPE("Europe"),
	NORTH_AMERICA("North America"),
	AFRICA("Africa"),
	OCEANIA("Oceania"),
	ANTARCTICA("Antarctica"),
	SOUTH_AMERICA("South America");
	
	private String description;

	private Continent(String description) {
		this.description = description;
	}
	
	public String description() {
		return description;
	}
	
	public static Continent of(String value) {
		if(isBlank(value)) throw new IllegalArgumentException("Unknown value: " + value);
		return Continent.valueOf(value.toUpperCase().replace(" ", "_"));
	}
	
}
