insert into status_grupo(nome) values('Pastorais'); 
insert into status_grupo(nome) values('Grupos'); 
insert into status_grupo(nome) values('Movimentos'); 

insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da criança', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da saúde', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da educação', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da juventude', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da comunicação', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da sobriedade', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral do menor', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da liturgia', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da catequese', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da familiar', now(), 1);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Pastoral da carcerária', now(), 1);

insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Encontro de Casais com Cristo', now(), 2);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Apostolado da Oração', now(), 2);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Renovação Carismática Católica', now(), 2);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Mãe Rainha', now(), 2);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Terço dos Homens', now(), 2);

insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Comunidade Canção Nova', now(), 3);
insert into grupo(nome, horaDoEncontro, id_status_grupo) values('Oração Paroquiais', now(), 3);
