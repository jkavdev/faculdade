package br.com.jkavdev.groupmanager.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "status_grupo")
public class StatusGrupo {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, length = 50)
	private String nome;
	
	protected StatusGrupo() { }

	public StatusGrupo(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

}
