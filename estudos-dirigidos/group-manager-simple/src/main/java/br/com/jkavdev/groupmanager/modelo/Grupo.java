package br.com.jkavdev.groupmanager.modelo;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Grupo {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 60)
	private String nome;

	@Column
	private LocalDateTime horaDoEncontro;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_status_grupo", nullable = false, foreignKey = @ForeignKey(name = "fk_grupo_id_status_grupo"))
	private StatusGrupo statusGrupo;
	
	public Grupo(String nome, StatusGrupo statusGrupo) {
		this.nome = nome;
		this.horaDoEncontro = LocalDateTime.now();
		this.statusGrupo = statusGrupo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public StatusGrupo getStatusGrupo() {
		return statusGrupo;
	}

	@Override
	public String toString() {
		return "Grupo [id=" + id + ", horaDoEncontro=" + horaDoEncontro + "]";
	}

}
