package br.com.jkavdev.groupmanager.test.persistence.modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.com.jkavdev.groupmanager.modelo.StatusGrupo;
import br.com.jkavdev.groupmanager.test.persistence.MainTest;

public class StatusGrupoTeste extends MainTest {

	@Test
	public void selecionarTodosStatusGrupos() {
		List<StatusGrupo> statusGrupos = getManager().createQuery("From StatusGrupo", StatusGrupo.class).getResultList();
		assertEquals(3, statusGrupos.size());
	}

	@Test
	public void inserirStatusGrupo() {
		EntityManager manager = getManager();
		StatusGrupo statusGrupo = new StatusGrupo("Dion");
		
		manager.getTransaction().begin();
		getManager().persist(statusGrupo);
		manager.getTransaction().commit();
		
		StatusGrupo statusGrupoSalvo = manager.createQuery("from StatusGrupo where nome = :nome", StatusGrupo.class)
			.setParameter("nome", statusGrupo.getNome())
			.getSingleResult();

		assumeTrue(statusGrupo.getNome().equals(statusGrupoSalvo.getNome()));
	}

}
