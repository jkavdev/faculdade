package br.com.jkavdev.groupmanager.test.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

public class MainTest {

	Logger log = Logger.getLogger(this.getClass().getName());

	private EntityManagerFactory factory;
	private EntityManager manager;

	@Before
	public void init() {
		factory = Persistence.createEntityManagerFactory("GroupManagerPU");
		manager = factory.createEntityManager();
	}

	@After
	public void close() {
		manager.close();
		factory.close();
	}
	
	public EntityManager getManager() {
		return manager;
	}

}
