package br.com.jkavdev.groupmanager.test.persistence.modelo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Test;

import br.com.jkavdev.groupmanager.modelo.Grupo;
import br.com.jkavdev.groupmanager.modelo.StatusGrupo;
import br.com.jkavdev.groupmanager.test.persistence.MainTest;

public class GrupoTeste extends MainTest {

	@Test
	public void selecionarTodosGrupos() {
		List<Grupo> grupos = getManager().createQuery("From Grupo", Grupo.class).getResultList();
		assertEquals(18, grupos.size());
	}

	@Test
	public void inserirGrupo() {
		EntityManager manager = getManager();
		
		StatusGrupo pastorais = manager.createQuery("from StatusGrupo where nome = :nomeStatus", StatusGrupo.class)
			.setParameter("nomeStatus", "Pastorais")
			.getSingleResult();

		manager.getTransaction().begin();
		Grupo grupo = new Grupo("Novo Pastoral da Sul", pastorais);
		manager.persist(grupo);
		manager.getTransaction().commit();
		
		Grupo grupoSalvo = manager.createQuery("from Grupo where nome = :nomeGrupo and statusGrupo = :statusGrupo", Grupo.class)
			.setParameter("nomeGrupo", grupo.getNome())
			.setParameter("statusGrupo", pastorais)
			.getSingleResult();
		
		assertEquals(grupo.getNome(), grupoSalvo.getNome());
		assertEquals(pastorais.getNome(), grupoSalvo.getStatusGrupo().getNome());
			
	}

}
